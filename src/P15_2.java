import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

class Student implements Comparable <Student>{

    String firstName;
    String lastName;
    int id;

    Student(String lastName, String firstName, int id){
        this.lastName = lastName;
        this.firstName = firstName;
        this.id = id;
    }

    public String getLastName(){
        return lastName;
    }

    public String getFirstName(){
        return firstName;
    }

    public int getID() {
        return id;
    }

    @Override
    public int compareTo(Student student) {
        int result = this.lastName.compareTo(student.lastName);
        if(result == 0){
            result = this.firstName.compareTo(student.firstName);
        }
        if (result == 0){
            result = Integer.compare(this.id, student.id);
        }
        return result;
    }

    @Override
    public String toString(){
        return firstName + " " + lastName + " " + "(ID = " + id + ") ";
    }
}

class StudentGradesByID {

    Scanner input = new Scanner(System.in);
    TreeMap<Student, String> students = new TreeMap<Student, String>();
    HashMap<Integer, Student> studentHashMap = new HashMap<Integer, Student>();
    Student student;

    public void menu(){
        System.out.println("Select an option: ");
        System.out.println("    (a) to add a student");
        System.out.println("    (r) to remove a student");
        System.out.println("    (m) to modify a grade");
        System.out.println("    (p) to print all grades");
        System.out.println("    (q) to quit");
        char selection = input.next().charAt(0);
        menuSelection(selection);
        menu();
    }

    public void menuSelection(char s){
        switch (s) {
            case 'a' -> addStudent();
            case 'r' -> removeStudent();
            case 'm' -> modifyGrade();
            case 'p' -> printAllGrades();
            case 'q' -> System.exit(0);
        }
    }
    private void addStudent() {


        Scanner input = new Scanner(System.in);
        System.out.print("Enter student first name: ");
        String firstName = input.next();
        System.out.print("Enter student last name: ");
        String lastName = input.next();
        System.out.print("Enter student ID: ");
        int id = input.nextInt();
        System.out.print("Enter student grade: ");
        String grade = input.next();


        students.put(student = new Student(lastName, firstName, id),grade);
        studentHashMap.put(id, student);

        System.out.println("Student added!\n");

    }

    private void removeStudent() {


        Scanner input = new Scanner(System.in);
        System.out.print("Enter student ID: ");
        int idInput = input.nextInt();

        if (studentHashMap.containsKey(idInput)) {
          students.remove(studentHashMap.get(idInput));
          studentHashMap.remove(idInput);
            System.out.println("Student removed!\n");
        }
        else{
            System.out.println("Student does not exist!\n");
        }
    }

    private void modifyGrade() {


        Scanner input = new Scanner(System.in);
        System.out.print("Enter student ID: ");
        int idInput = input.nextInt();

        if (studentHashMap.containsKey(idInput)){
            System.out.print("Enter student grade: ");
            String grade = input.next();
            students.put(studentHashMap.get(idInput),grade);

            System.out.print("Student modified!\n");
        }
        else{
            System.out.print("Student doesn't exist!\n");
        }

    }

    private void printAllGrades() {

       for (Map.Entry<Student,String> entry : students.entrySet())
           System.out.println(entry.getKey().getFirstName() + " " + entry.getKey().getLastName() + " "
           + "(ID=" + entry.getKey().getID() + ") : " + entry.getValue());
    }

}


public class P15_2 {

    public static void main(String[] args) {

        StudentGradesByID school = new StudentGradesByID();
        school.menu();

    }
}
