import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

class StudentGrades {

    Scanner input = new Scanner(System.in);
    TreeMap<String, String> students = new TreeMap<String, String>();

    public void menu(){
            System.out.println("Select an option: ");
            System.out.println("    (a) to add a student");
            System.out.println("    (r) to remove a student");
            System.out.println("    (m) to modify a grade");
            System.out.println("    (p) to print all grades");
            System.out.println("    (q) to quit");
            char selection = input.next().charAt(0);
            menuSelection(selection);
            menu();
    }

    public void menuSelection(char s){
        switch (s){
            case 'a': addStudent();
                break;
            case 'r': removeStudent();
                break;
            case 'm': modifyGrade();
                break;
            case 'p': printAllGrades();
                break;
            case 'q':System.exit(0);
        }
    }
    private void addStudent() {


        Scanner input = new Scanner(System.in);
        System.out.println("Enter student name: ");
        String name = input.next();
        System.out.println("Enter student grade: ");
        String grade = input.next();

        students.put(name,grade);

        System.out.println("Student added!");

    }

    private void removeStudent() {


        Scanner input = new Scanner(System.in);
        System.out.println("Enter student name: ");
        String name = input.next();

        if(students.containsKey(name)){
            students.remove(name);

            System.out.println("Student removed!");
        }
        else{
            System.out.println("Student doesn't exist!");
        }
    }

    private void modifyGrade() {


        Scanner input = new Scanner(System.in);
        System.out.println("Enter student name: ");
        String name = input.next();

        if(students.containsKey(name)){
            System.out.println("Enter student grade: ");
            String grade = input.next();

            students.put(name,grade);

            System.out.println("Student modified!");
        }
        else{
            System.out.println("Student doesn't exist!");
        }

    }

    private void printAllGrades() {

        for (Map.Entry<String,String> entry : students.entrySet())
            System.out.println(entry.getKey() +
                    " : " + entry.getValue());
    }

}


public class E15_4 {
    public static void main(String[] args) {

        StudentGrades school = new StudentGrades();
        school.menu();

    }
}
