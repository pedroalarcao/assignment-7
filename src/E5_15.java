import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Map;
import java.util.LinkedHashMap;
import java.io.LineNumberReader;

public class E5_15 {
    public static void main(String[] args) throws FileNotFoundException {

        Map<String, ArrayList<String>> map = new LinkedHashMap<String, ArrayList<String>>();

        Scanner javaFile = new Scanner(new File("Main.java"));

        while(javaFile.hasNextLine()) {

            String line = javaFile.nextLine();
            System.out.println(line);

            Scanner input = new Scanner(line);
            input.useDelimiter("[^A-Za-z0-9_]+");

            while(input.hasNext())
            {
                //traversing each identifier in the line
                String identifier = input.next();

                // updating the value of each token in the map
                ArrayList<String> lines = map.getOrDefault(identifier, new ArrayList<String>());
                lines.add(line);
                map.put(identifier, lines);
            }

            input.close();
        }

        javaFile.close();

        int index = 0;
        //traversing map and printing identifiers
        for(String key : map.keySet())
        {

            System.out.println(index + ": " + key + " occurs in:");

            //traverses each time key is seen on map
            for(String line : map.get(key))
            {
                System.out.println(line);
            }

            index++;
        }
    }
}